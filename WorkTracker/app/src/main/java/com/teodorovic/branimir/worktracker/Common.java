package com.teodorovic.branimir.worktracker;

import java.util.Date;
import java.util.GregorianCalendar;


public class Common {
    // convert time in seconds to a String in 00:00:00 format
    public static String toText(int time){
        int hours = time / 3600;
        int minutes = (time % 3600) / 60;
        int seconds = time % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    // get difference between two dates in days
    public static String getDateDiff(){
        MyDate start = App.getCurrentSubject().getStarted();
        Date d1 = new GregorianCalendar(start.getYear(), start.getMonth()-1, start.getDay()).getTime();
        long diff = new Date(System.currentTimeMillis()).getTime() - d1.getTime();
        return String.valueOf((diff / (1000 * 60 * 60 * 24)) + 1);
    }
}
