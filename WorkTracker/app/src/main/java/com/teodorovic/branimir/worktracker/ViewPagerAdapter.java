package com.teodorovic.branimir.worktracker;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new SubjectFragment();
            case 1:
                return new HistoryFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "today";
            case 1:
                return "history";
        }

        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
