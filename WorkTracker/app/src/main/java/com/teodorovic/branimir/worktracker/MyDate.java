package com.teodorovic.branimir.worktracker;

import java.util.Calendar;
import java.util.regex.Pattern;

public class MyDate {

    @Override
    public String toString() {
        return String.valueOf(day) + "." + String.valueOf(month) + "." + String.valueOf(year);
    }

    public int year, month, day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate(){}

    public MyDate(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);

        this.year = calendar.get(Calendar.YEAR);
        this.month = calendar.get(Calendar.MONTH) + 1;
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    public MyDate(String s){
        MyDate result = new MyDate();
        this.year = Integer.parseInt(s.split(Pattern.quote("."))[2]);
        this.month = Integer.parseInt(s.split(Pattern.quote("."))[1]);
        this.day = Integer.parseInt(s.split(Pattern.quote("."))[0]);

    }

    public long toMillis(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTimeInMillis();
    }

    @Override
    public boolean equals(Object o) {
        MyDate date = (MyDate) o;
        return (year==date.year && month==date.month && day==date.day) ? true : false;
    }

}
