package com.teodorovic.branimir.worktracker;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.teodorovic.branimir.worktracker.model.Day;

import de.greenrobot.event.EventBus;

public class ViewDayDialog extends DialogFragment {
    private View form = null;
    private Day day;
    //String date;

    public ViewDayDialog() {}

    //public void setDate(String date){
    //    this.date = date;
    //}

    public void setDay(Day day){
        this.day = day;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //day = Day.load(new MyDate(date), App.getCurrentSubject());

        if(day != null) {
            form = getActivity().getLayoutInflater().inflate(R.layout.dialog_view_day, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            TextView date = (TextView) form.findViewById(R.id.date);
            TextView time = (TextView) form.findViewById(R.id.time);
            TextView note = (TextView) form.findViewById(R.id.note);

            if (day != null) {
                //date.setText(day.getDate().toString());
                time.setText(Common.toText(day.getTime()));
                note.setText(day.getNote());
            }
            return builder.setTitle(day.getDate().toString()).setView(form)
                    .setNegativeButton(android.R.string.ok, null).create();
        }
        return null;
    }
}
