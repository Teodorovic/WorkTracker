package com.teodorovic.branimir.worktracker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.teodorovic.branimir.worktracker.model.Subject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, EditSubjectDialog.OnSubjectSaved {

    private ArrayList<Subject> items;
    ListView list;
    private int currentSubjectId;
    private SubjectAdapter adapter;
    public static final String NOTIFICATION = "notification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Subjects");
        items = new ArrayList<Subject>();
        Subject.loadAll(items);
        list = (ListView) findViewById(android.R.id.list);
        list.setOnItemClickListener(this);
        adapter = new SubjectAdapter();
        list.setAdapter(adapter);
        registerForContextMenu(list);
        list.setOnItemLongClickListener(this);

        if(items.size() == 0)
            new EditSubjectDialog("Add new subject").show(getSupportFragmentManager(), "");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (prefs.getBoolean("launchQuote", false)) {
            String quote = getIntent().getStringExtra(NOTIFICATION);
            Intent i = new Intent(getApplicationContext(), MotivationActivity.class);
            if(!TextUtils.isEmpty(quote))
                i.putExtra(NOTIFICATION, quote);

            startActivity(i);
        }


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId() == android.R.id.list){
            menu.add(Menu.NONE, 0, 0, R.string.edit);
            menu.add(Menu.NONE, 1, 1, R.string.delete);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:  // edit
                EditSubjectDialog dialog = new EditSubjectDialog("Edit subject");
                dialog.setId(currentSubjectId);
                dialog.show(getSupportFragmentManager(), "");
                break;
            case 1: // delete
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                Subject.delete(currentSubjectId);
                                onSubjectSaved();
                                if(App.getCurrentSubject() != null && currentSubjectId == App.getCurrentSubject().getId()){
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    prefs.edit().putInt(Subject.PREF, -1).commit();
                                    App.setCurrentSubject(null);
                                }
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(R.string.delete_question).setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener).show();

                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                new EditSubjectDialog("Add new subject").show(getSupportFragmentManager(), "");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        currentSubjectId =view.getId();
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        App.setCurrentSubject(Subject.load(view.getId()));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.edit().putInt(Subject.PREF, view.getId()).commit();

        startActivity(new Intent(this, SubjectActivity.class));
    }

    @Override
    public void onSubjectSaved() {
        Subject.loadAll(items);
        adapter.notifyDataSetChanged();
    }

    private class SubjectAdapter extends ArrayAdapter<Subject> {

        public SubjectAdapter() {
            super(MainActivity.this, R.layout.subject_row, R.id.name, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.subject_row, parent, false);

            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView desc = (TextView) convertView.findViewById(R.id.desc);
            TextView started = (TextView) convertView.findViewById(R.id.started);

            Subject subject = getItem(position);

            name.setText(subject.getName());
            desc.setText(subject.getDesc());
            started.setText(subject.getStarted().toString());

            convertView.setId(getItem(position).getId());

            return convertView;
        }
    }

}
