package com.teodorovic.branimir.worktracker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;

public class Receiver extends BroadcastReceiver {
    public Receiver() {
    }

    // activates on device boot
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.getBoolean("showNotification", false)) {

            // create alarm for 6PM o'clock every day
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 18);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 10);

            long startAt = calendar.getTimeInMillis();
            if(System.currentTimeMillis() > startAt)
                startAt += 24*60*60*1000;

            PendingIntent pi = PendingIntent.getService(context, 0 , new Intent(context, NotificationService.class),PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, startAt,
                    AlarmManager.INTERVAL_DAY, pi);
        }
    }
}
