package com.teodorovic.branimir.worktracker;


public class TimerTickEvent {
    private int time;
    public TimerTickEvent(int time){
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
