package com.teodorovic.branimir.worktracker;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.teodorovic.branimir.worktracker.model.Day;

import java.util.Calendar;

import de.greenrobot.event.EventBus;

public class SubjectFragment extends Fragment implements View.OnClickListener {

    private TextView tvDay, tvProgress, tvTotal, tvTimer, tvNote;
    private boolean running = false;
    int time = 0;
    Day day;
    Calendar myCal;
    Intent serviceIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        serviceIntent = new Intent(getActivity().getApplicationContext(), TimerService.class);

        myCal = Calendar.getInstance();


        day = Day.load(new MyDate(System.currentTimeMillis()), App.getCurrentSubject());
        getActivity().setTitle(App.getCurrentSubject().getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subject, container, false);
        tvDay = (TextView) v.findViewById(R.id.day);
        tvProgress = (TextView) v.findViewById(R.id.progress);
        tvTimer = (TextView) v.findViewById(R.id.timer);
        tvTotal = (TextView) v.findViewById(R.id.total);
        tvNote = (TextView) v.findViewById(R.id.tvNote);
        tvNote.setOnClickListener(this);

        tvTimer.setOnClickListener(this);

        if (day != null) {
            tvProgress.setText("Today: " + Common.toText(day.getTime()));

            if (day.getNote() != null && day.getNote().length() > 0)
                tvNote.setText("Note: " + day.getNote());
        }

        tvDay.setText("Day: " + Common.getDateDiff());
        tvTotal.setText("Total: " + Common.toText(Day.getSum(App.getCurrentSubject().getId())));

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.timer:
                if (running) { // stop
                    // stop service
                    if (getActivity() != null) {
                        try {
                            //Intent i = new Intent(getActivity(), TimerService.class);
                            //i.putExtra("action", "stop");
                            //WakefulIntentService.sendWakefulWork(getActivity(), i);
                            getActivity().stopService(serviceIntent);
                        } catch (Exception e) { }
                    }
                    if (day == null) {
                        day = new Day();
                        day.setSubject(App.getCurrentSubject().getId());
                        day.setTime(time);
                        day.setDate(new MyDate(System.currentTimeMillis()));
                        day.setId(day.save());
                    } else {
                        day.setTime(day.getTime() + time);
                        day.update();
                    }
                    EventBus.getDefault().post(new TimerStopedEvent());
                    time = 0;
                    tvTimer.setText(R.string.start_time);
                } else { // start
                    // start service
                    if (getActivity() != null) {
                        try {
                            //Intent i = new Intent(getActivity(), TimerService.class);
                            //i.putExtra("action", "start");
                            //WakefulIntentService.sendWakefulWork(getActivity(), i);
                            getActivity().startService(serviceIntent);
                        } catch (Exception e) { }
                    }
                }
                running = !running;

                break;
            case R.id.tvNote:
                EditNoteDialog dialog = new EditNoteDialog();
                if (day != null)
                    dialog.setDate(day.getDate().toString());
                else
                    dialog.setDate(new MyDate(System.currentTimeMillis()).toString());
                dialog.show(getActivity().getSupportFragmentManager(), "");

                break;
        }
    }

    @Override
    public void onDetach() {
        if (day != null) {
            day.setTime(day.getTime() + time);
            day.update();
        }
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        if (getActivity() != null) {
            try {
                //Intent i = new Intent(getActivity(), TimerService.class);
                //i.putExtra("action", "stop");
                //WakefulIntentService.sendWakefulWork(getActivity(), i);
                getActivity().stopService(serviceIntent);
            } catch (Exception e) { }
        }
        super.onDestroy();
    }

    public void onEventMainThread(DaySavedEvent event) {
        day = Day.load(new MyDate(System.currentTimeMillis()), App.getCurrentSubject());
        tvNote.setText("Note: " + event.getDay().getNote());
    }

    public void onEventMainThread(TimerTickEvent event) {
        this.time = event.getTime();
        tvTimer.setText(Common.toText(time));
        if (day != null) {
            tvProgress.setText("Today: " + Common.toText(day.getTime() + time));
        } else {
            tvProgress.setText("Today: " + Common.toText(time));
        }

        tvTotal.setText("Total: " + Common.toText(Day.getSum(App.getCurrentSubject().getId()) + time));

        myCal.setTimeInMillis(System.currentTimeMillis());
        if (myCal.get(Calendar.HOUR_OF_DAY) == 0 && myCal.get(Calendar.MINUTE) == 0 && myCal.get(Calendar.SECOND) == 0) {
            // save todays work and start new
            day.setTime(day.getTime() + time);
            day.update();

            day = new Day();
            day.setSubject(App.getCurrentSubject().getId());
            day.setDate(new MyDate(System.currentTimeMillis()));
            day.setId(day.save());

            tvDay.setText("Day: " + Common.getDateDiff());
            tvNote.setText(getString(R.string.add_note));
            time = 0;
            tvTimer.setText(Common.toText(time));
            tvProgress.setText("Today: " + Common.toText(day.getTime()));

            Toast.makeText(getActivity(), getString(R.string.new_day), Toast.LENGTH_SHORT).show();
        }
    }
}
