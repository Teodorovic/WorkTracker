package com.teodorovic.branimir.worktracker;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.teodorovic.branimir.worktracker.model.Day;

import de.greenrobot.event.EventBus;


public class EditNoteDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private View form = null;
    private Day day;
    String date;

    public EditNoteDialog() {}

    public void setDate(String date){
        this.date = date;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        day = Day.load(new MyDate(date), App.getCurrentSubject());

        form = getActivity().getLayoutInflater().inflate(R.layout.dialog_note, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        EditText note = (EditText) form.findViewById(R.id.note);

        if(day != null){
            note.setText(day.getNote());
        }

        return builder.setTitle(R.string.note_dialog_title).setView(form)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null).create();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        EditText note = (EditText) form.findViewById(R.id.note);
        if (day == null) {
            day = new Day();
            day.setNote(note.getText().toString());
            day.setTime(0);
            day.setDate(new MyDate(System.currentTimeMillis()));
            day.setSubject(App.getCurrentSubject().getId());
            day.setId(day.save());
        }
        else {
            day.setNote(note.getText().toString());
            day.update();
        }

        EventBus.getDefault().post(new DaySavedEvent(day));
    }

}
