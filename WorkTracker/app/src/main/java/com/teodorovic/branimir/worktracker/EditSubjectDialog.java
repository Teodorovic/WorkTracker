package com.teodorovic.branimir.worktracker;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.teodorovic.branimir.worktracker.model.Subject;

public class EditSubjectDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private View form = null;
    private int id = -1;
    private String title;

    public EditSubjectDialog(String title){
        this.title = title;
    }

    public void setId(int id){
        this.id = id;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        form = getActivity().getLayoutInflater().inflate(R.layout.dialog_subject, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        EditText name = (EditText) form.findViewById(R.id.name);
        EditText desc = (EditText) form.findViewById(R.id.desc);

        if(id != -1){
            Subject subject = Subject.load(id);
            name.setText(subject.getName());
            desc.setText(subject.getDesc());
        }


        return builder.setTitle(title).setView(form)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null).create();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        EditText name = (EditText) form.findViewById(R.id.name);
        EditText desc = (EditText) form.findViewById(R.id.desc);
        Subject subject;
        if (id == -1) {
            subject = new Subject();
            subject.setName(name.getText().toString());
            subject.setDesc(desc.getText().toString());
            subject.save();
        }
        else {
            subject = Subject.load(id);
            subject.setName(name.getText().toString());
            subject.setDesc(desc.getText().toString());
            subject.update();
        }

        OnSubjectSaved activity = (OnSubjectSaved) getActivity();
        activity.onSubjectSaved();
    }

    public interface OnSubjectSaved {
        void onSubjectSaved();
    }
}
