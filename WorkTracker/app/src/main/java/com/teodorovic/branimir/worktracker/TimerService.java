package com.teodorovic.branimir.worktracker;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;

import de.greenrobot.event.EventBus;

public class TimerService extends Service {
    private Handler handler;
    private Runnable timer;
    private int seconds;
    private PowerManager powerManager;
    private PowerManager.WakeLock wl;

    @Override
    public void onCreate() {
        super.onCreate();

        // acquire wakelock
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "lock");
        wl.acquire();

        seconds = 0;
        handler = new Handler();
        timer = new Runnable() {
            @Override
            public void run() {
                seconds++;
                EventBus.getDefault().post(new TimerTickEvent(seconds));
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(timer, 1000);
    }

    @Override
    public void onDestroy() {
        // release wakelock
        if(wl!=null){
            wl.release();
            wl = null;
        }

        if (timer!=null)
            handler.removeCallbacks(timer);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
