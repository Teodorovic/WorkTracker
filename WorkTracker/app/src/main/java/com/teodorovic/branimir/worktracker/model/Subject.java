package com.teodorovic.branimir.worktracker.model;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.teodorovic.branimir.worktracker.App;
import com.teodorovic.branimir.worktracker.MyDate;

import java.util.ArrayList;

public class Subject {
    public static final String TABLE_NAME = "subject";
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_DESC = "description";
    public static final String COL_STARTED = "started";

    public static final String PREF = "subject";

    private int id;
    private String name;
    private String desc;
    private MyDate started;

    public Subject(){}

    public Subject(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MyDate getStarted() {
        return started;
    }

    public void setStarted(MyDate started){
        this.started = started;
    }

    public long save() {
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, name);
        if(desc != null){
            cv.put(COL_DESC, desc);
        }
        cv.put(COL_STARTED, new MyDate(System.currentTimeMillis()).toString());

        return App.getDatabase().insert(TABLE_NAME, null, cv);
    }

    public static int delete(int id){
        SQLiteDatabase db = App.getDatabase();
        Day.deleteBySubject(id);
        return db.delete(TABLE_NAME, COL_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public boolean update(){
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, name);
        cv.put(COL_DESC, desc);
        return App.getDatabase().update(TABLE_NAME, cv, COL_ID + " = ?", new String[]{String.valueOf(id)}) == 1 ? true : false;
    }

    public static Subject load(int id){
        Subject subject = new Subject(id);
        Cursor c = App.getDatabase().query(TABLE_NAME, null, COL_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        try {
            if(c.moveToFirst()){
                subject.setName(c.getString(c.getColumnIndex(COL_NAME)));
                subject.setDesc(c.getString(c.getColumnIndex(COL_DESC)));
                subject.setStarted(new MyDate(c.getString(c.getColumnIndex(COL_STARTED))));
            }
        } finally {
            c.close();
        }
        return subject;
    }

    public static void loadAll(ArrayList<Subject> list){
        list.clear();
        Cursor c = App.getDatabase().query(TABLE_NAME, null, null, null, null, null, null);

        if(c.moveToFirst()){
            do {
                Subject s = new Subject(c.getInt(c.getColumnIndex(COL_ID)));
                s.setName(c.getString(c.getColumnIndex(COL_NAME)));
                s.setDesc(c.getString(c.getColumnIndex(COL_DESC)));
                s.setStarted(new MyDate(c.getString(c.getColumnIndex(COL_STARTED))));
                list.add(s);
            } while (c.moveToNext());
        }
    }

    static String getSql(){
        return "CREATE TABLE " + TABLE_NAME + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME + " TEXT, " + COL_DESC +
                " TEXT, " + COL_STARTED + " TEXT);";
    }
}
