package com.teodorovic.branimir.worktracker;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.teodorovic.branimir.worktracker.model.DbHelper;
import com.teodorovic.branimir.worktracker.model.Subject;

import java.util.Calendar;

public class App extends Application {
    private static SQLiteDatabase db;

    public static SQLiteDatabase getDatabase(){
        return db;
    }

    private static Subject currentSubject;

    public static Subject getCurrentSubject() {
        return currentSubject;
    }

    public static void setCurrentSubject(Subject currentSubject) {
        App.currentSubject = currentSubject;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = new DbHelper(this).getWritableDatabase();

        // create alarm for 6PM o'clock every day
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 10);

        long startAt = calendar.getTimeInMillis();
        if(System.currentTimeMillis() > startAt)
            startAt += 24*60*60*1000;

        PendingIntent pi = PendingIntent.getService(this, 0 , new Intent(this, NotificationService.class),PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, startAt,
                AlarmManager.INTERVAL_DAY, pi);
    }
}
