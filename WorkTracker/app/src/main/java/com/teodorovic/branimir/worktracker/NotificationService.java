package com.teodorovic.branimir.worktracker;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.teodorovic.branimir.worktracker.model.Day;

import java.util.Random;

public class NotificationService extends IntentService {


    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(!Day.checkTodayEntries()) {
            String[] quotes =  getResources().getStringArray(R.array.quotes);
            String quote = quotes[new Random().nextInt(quotes.length)];

            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.no_work_today))
                    .setContentText("\"" + quote + "\"")
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText("\"" + quote + "\"").setBigContentTitle(getString(R.string.no_work_today)));

            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(MainActivity.NOTIFICATION, quote);
            PendingIntent pi = PendingIntent.getActivity(this,0,i, 0);
            mBuilder.setContentIntent(pi);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
