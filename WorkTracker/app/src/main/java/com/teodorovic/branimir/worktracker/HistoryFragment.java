package com.teodorovic.branimir.worktracker;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.teodorovic.branimir.worktracker.model.Day;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class HistoryFragment extends Fragment implements AdapterView.OnItemLongClickListener {

    private ArrayList<Day> items;
    ListView list;
    private DayAdapter adapter;
    TextView highest, average;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_history, container, false);

        items = new ArrayList<Day>();
        Day.loadAll(App.getCurrentSubject(), items);

        list = (ListView) v.findViewById(android.R.id.list);
        adapter = new DayAdapter();
        list.setAdapter(adapter);
        registerForContextMenu(list);
        list.setOnItemLongClickListener(this);

        highest = (TextView) v.findViewById(R.id.highest);
        average = (TextView) v.findViewById(R.id.average);
        highest.setText("Highest: " + Common.toText(Day.getHighest(App.getCurrentSubject().getId())));
        average.setText("Average: " + Common.toText(Day.getAverage(App.getCurrentSubject().getId())));

        return v;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ViewDayDialog dialog = new ViewDayDialog();
        dialog.setDay((Day)view.getTag());
        dialog.show(getActivity().getSupportFragmentManager(), "");

        return false;
    }

    public void refresh(){
        Day.loadAll(App.getCurrentSubject(), items);
        adapter.notifyDataSetChanged();		
        highest.setText("Highest: " + Common.toText(Day.getHighest(App.getCurrentSubject().getId())));
        average.setText("Average: " + Common.toText(Day.getAverage(App.getCurrentSubject().getId())));
    }

    private class DayAdapter extends ArrayAdapter<Day> {

        public DayAdapter() {
            super(getActivity(), R.layout.subject_row, R.id.name, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.day_row, parent, false);

            TextView tvDate = (TextView) convertView.findViewById(R.id.date);
            TextView tvDesc = (TextView) convertView.findViewById(R.id.desc);
            TextView tvTime = (TextView) convertView.findViewById(R.id.time);

            Day day = getItem(position);

            tvDate.setText(day.getDate().toString());

            String desc = day.getNote();
            if(desc != null && desc.length() > 30)
                tvDesc.setText(desc.substring(0, 30) + "...");
            else
                tvDesc.setText(desc);

            tvTime.setText(Common.toText(day.getTime()));

            convertView.setTag(day);

            return convertView;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }
    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    public void onEventMainThread(TimerStopedEvent event) {
        refresh();
    }

    public void onEventMainThread(DaySavedEvent event) {
        refresh();
    }
}
