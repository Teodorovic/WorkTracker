package com.teodorovic.branimir.worktracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MotivationActivity extends AppCompatActivity {
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivation);

        tv = (TextView) findViewById(R.id.text);

        String quote = getIntent().getStringExtra(MainActivity.NOTIFICATION);
        if(!TextUtils.isEmpty(quote))
            tv.setText("\"" + quote + "\"");
        else {
            String[] quotes = getResources().getStringArray(R.array.quotes);
            tv.setText("\"" + quotes[new Random().nextInt(quotes.length)] + "\"");
        }
    }

    public void dismiss(View v) {
        finish();
    }
}
