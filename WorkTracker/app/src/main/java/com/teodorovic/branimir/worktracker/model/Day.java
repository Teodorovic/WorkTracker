package com.teodorovic.branimir.worktracker.model;


import android.content.ContentValues;
import android.database.Cursor;
import com.teodorovic.branimir.worktracker.App;
import com.teodorovic.branimir.worktracker.MyDate;

import java.util.ArrayList;

public class Day {
    private static final String TABLE_NAME = "daydata";

    public static final String COL_ID = "_id";
    public static final String COL_SUBJECT = "subject";
    public static final String COL_DATE = "date";
    public static final String COL_TIME = "time";
    public static final String COL_NOTE = "note";

    private long id;
    private int subject;
    private MyDate date;
    private int time;
    private String note;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public MyDate getDate() {
        return date;
    }

    public void setDate(MyDate date) {
        this.date = date;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note){
        this.note = note;
    }
    public long save(){
        ContentValues cv = new ContentValues();
        cv.put(COL_SUBJECT, subject);
        String s = date.toString();
        cv.put(COL_DATE, date.toString());
        cv.put(COL_TIME, time);
        if(note != null)
            cv.put(COL_NOTE, note);

        return App.getDatabase().insert(TABLE_NAME, null, cv);
    }

    public boolean update() {
        ContentValues cv = new ContentValues();
        cv.put(COL_SUBJECT, subject);
        String s = date.toString();
        cv.put(COL_DATE, date.toString());
        cv.put(COL_TIME, time);
        if(note != null)
            cv.put(COL_NOTE, note);

        return App.getDatabase().update(TABLE_NAME, cv, COL_ID + " = ?", new String[]{String.valueOf(id)})
                == 1 ? true : false;
    }

    public int delete() {
        return App.getDatabase().delete(TABLE_NAME, COL_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public static Day load(MyDate date, Subject subject){
        Day result = null;

        Cursor c = App.getDatabase().query(TABLE_NAME, null,
                COL_DATE + " = ? AND " + COL_SUBJECT + " = ?", new String[] {date.toString(), String.valueOf(subject.getId())}, null, null, null);
        if(c.moveToFirst()){
            result = new Day();
            result.setId(c.getInt(c.getColumnIndex(COL_ID)));
            result.setSubject(c.getInt(c.getColumnIndex(COL_SUBJECT)));
            result.setDate(new MyDate(c.getString(c.getColumnIndex(COL_DATE))));
            result.setTime(c.getInt(c.getColumnIndex(COL_TIME)));
            result.setNote(c.getString(c.getColumnIndex(COL_NOTE)));
        }
        c.close();
        return result;
    }

    public static int deleteBySubject(int id){
        return App.getDatabase().delete(TABLE_NAME, COL_SUBJECT + " = ?", new String[]{String.valueOf(id)});
    }

    public static int getSum(int subjectId){
        int result = 0;
        Cursor c = App.getDatabase().query(TABLE_NAME, new String[] {"sum(" + COL_TIME + ")"}, COL_SUBJECT + " = ?", new String[] {String.valueOf(subjectId)}, null, null, null);
        if(c.moveToFirst()){
            result = c.getInt(0);
        }
        c.close();
        return result;
    }

    static String getSql(){
        return "CREATE TABLE " + TABLE_NAME + " (" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_SUBJECT +" INTEGER, " +
                COL_DATE + " TEXT, " + COL_TIME + " INTEGER, " + COL_NOTE + " TEXT, FOREIGN KEY(subject) REFERENCES subject(_id));";
    }

    public static void loadAll(Subject subject, ArrayList<Day> list) {
        list.clear();
        Cursor c = App.getDatabase().query(TABLE_NAME, null, COL_SUBJECT + " = ?", new String[] { String.valueOf(subject.getId()) }, null, null, COL_ID + " DESC");

        if(c.moveToFirst()){
            do {
                Day d = new Day();
                d.setId(c.getInt(c.getColumnIndex(COL_ID)));
                d.setSubject(c.getInt(c.getColumnIndex(COL_SUBJECT)));
                d.setDate(new MyDate(c.getString(c.getColumnIndex(COL_DATE))));
                d.setTime(c.getInt(c.getColumnIndex(COL_TIME)));
                d.setNote(c.getString(c.getColumnIndex(COL_NOTE)));
                list.add(d);
            } while (c.moveToNext());
        }
        c.close();
    }

    public static boolean checkTodayEntries(){
        String today = new MyDate(System.currentTimeMillis()).toString();
        Cursor c = App.getDatabase().query(TABLE_NAME, null, COL_DATE + " = ?", new String[]{today}, null, null, null);
        boolean result = c.getCount() > 0 ? true : false;
        c.close();
        return result;
    }


    public static int getAverage(int subjectId) {
        int result = 0;
        Cursor c = App.getDatabase().query(TABLE_NAME, new String[] {"avg(" + COL_TIME + ")"}, COL_SUBJECT + " = ?", new String[] {String.valueOf(subjectId)}, null, null, null);
        if(c.moveToFirst()){
            result = c.getInt(0);
        }
        c.close();
        return result;
    }

    public static int getHighest(int subjectId) {
        int result = 0;
        Cursor c = App.getDatabase().query(TABLE_NAME, new String[] {"max(" + COL_TIME + ")"}, COL_SUBJECT + " = ?", new String[] {String.valueOf(subjectId)}, null, null, null);
        if(c.moveToFirst()){
            result = c.getInt(0);
        }
        c.close();
        return result;
    }
}
