package com.teodorovic.branimir.worktracker;

import com.teodorovic.branimir.worktracker.model.Day;


public class DaySavedEvent {
    private Day day;
    public DaySavedEvent(Day day) {
        this.day = day;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }
}
